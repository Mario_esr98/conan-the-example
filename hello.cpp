#include <iostream>

#include "hello.h"


void hello() {
    #ifdef NDEBUG
    std::cout << "Hello World Release!" <<std::endl;
    std::cout << "Greetings from Monterrey" <<std::endl;
    #else
    std::cout << "Hello World Debug!" <<std::endl;
    #endif
}
